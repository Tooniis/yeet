extends Node

signal ready_to_start
signal player_spawned
signal info_available

const MOUSE_SENSITIVITY := 0.2
const MOUSE_SMOOTHING := 0.8
const MIN_FOV := 40
const MAX_FOV := 80
const FOV_STEP := 5
const WALK_STRENGTH := 0.5

# Scenes
var scene: Node
var camera_translation := Vector3.ZERO

var platform_is_mobile: bool = true if OS.get_name() == "Android" else false
var event := InputEventAction.new()

var forward_action := InputEventAction.new()
var backward_action := InputEventAction.new()
var left_action := InputEventAction.new()
var right_action := InputEventAction.new()

var action = {
	forward = false,
	backward = false,
	left = false,
	right = false,
	sprint = false,
}

# Network
var offline: bool
var peer := NetworkedMultiplayerENet.new()
var ip := ""
var port: int
var info := {
	name = "",
	spawn = Vector3(),
}
remote var infos := {}

var looking_at := 0

# joystick
var joystick_touch_index := -1


func _init():
	event.action = "quit"
	forward_action.action = "forward"
	backward_action.action = "backward"
	left_action.action = "left"
	right_action.action = "right"


func loaded():
	scene = $"../island"
	
	if platform_is_mobile:
		get_tree().set_screen_stretch(
			SceneTree.STRETCH_MODE_VIEWPORT,
			SceneTree.STRETCH_ASPECT_KEEP_HEIGHT,
			Vector2(
				ProjectSettings.get_setting("display/window/size/width"),
				ProjectSettings.get_setting("display/window/size/height")
			)
		)
		
	
	# Network
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_to_server")
	get_tree().connect("connection_failed", self, "_connection_failed")


func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST:
		event.pressed = true
		Input.parse_input_event(event)
		event.pressed = false
		Input.parse_input_event(event)


func _unhandled_key_input(event: InputEventKey):
	if not event.is_echo():
		var is_pressed = event.is_pressed()
		if event.is_action("kb_forward"): action.forward = is_pressed
		elif event.is_action("kb_backward"): action.backward = is_pressed
		elif event.is_action("kb_left"): action.left = is_pressed
		elif event.is_action("kb_right"): action.right = is_pressed
		elif event.is_action("sprint"): action.sprint = is_pressed
		else: return
		
		update_movement_actions()


func update_movement_actions():
	var strength := WALK_STRENGTH
	var vector := Vector2.ZERO
	
	if action.sprint:
		strength = 1.0
	
	if action.forward:
		vector.y = 1.0
	if action.backward:
		vector.y = -1.0
	if action.left:
		vector.x = -1.0
	if action.right:
		vector.x = 1.0
	
	parse_movement_events(vector.normalized() * strength)
	print(vector.normalized() * strength)


func parse_movement_events(vector: Vector2):
	# Forward
	forward_action.pressed = vector.y > 0.0
	forward_action.strength = clamp(vector.y, 0.0, 1.0)
	Input.parse_input_event(forward_action)
	# Backward
	backward_action.pressed = vector.y < 0.0
	backward_action.strength = -clamp(vector.y, -1.0, 0.0)
	Input.parse_input_event(backward_action)
	# Left
	left_action.pressed = vector.x < 0.0
	left_action.strength = -clamp(vector.x, -1.0, 0.0)
	Input.parse_input_event(left_action)
	# Right
	right_action.pressed = vector.x > 0.0
	right_action.strength = clamp(vector.x, 0.0, 1.0)
	Input.parse_input_event(right_action)


func _ip_obtained(obtained_ip: String, obtained_port: int):
	ip = obtained_ip
	port = obtained_port


func _name_obtained(name: String):
	info.name = name
	offline = false
	peer.create_client(ip, port)
	get_tree().network_peer = peer


func _offline_mode_selected():
	offline = true
	peer.create_server(0)
	get_tree().network_peer = peer
	infos[get_id()] = {name = "", spawn = Vector3(0.0, 12.0, -3.0)}
	emit_signal("ready_to_start")


func _player_connected(id: int):
	emit_signal("info_available", "Player " + str(id) + " connected")


remote func _player_registered(id: int):
	if id == get_id():
		# Retrieve my info
		info = infos[id]
		emit_signal("ready_to_start")
	else:
		emit_signal("player_spawned", id)


func _player_disconnected(id: int):
	emit_signal("info_available", "Player " + str(id) + " disconnected")


func _connected_to_server():
	emit_signal("info_available", "Connected")
	print("Connected")
	rpc_id(1, "register_player", info)


func _connection_failed():
	emit_signal("info_available", "Connection failed")
	print("Connection failed")


func get_id():
	return get_tree().get_network_unique_id()
