extends RigidBody

signal transform_changed

onready var ground = get_node("../land")
onready var camera = $man/armature/Skeleton/head/camera

var target_rotation = rotation
var force_amount = 200000.0
var ground_contact = false
var jump_cooldown = 0.0

var last_transform = Transform()
var target_transform = last_transform
var transform_change_counter = 0.0
var transform_interpolation_counter = 0.0

var action = {
	forward = false,
	backward = false,
	left = false,
	right = false,
	sprint = false,
	jump = false
}

func _ready():
	# Connect scene to translation signal
	self.connect("transform_changed", get_parent(), "_player_transform_changed")
	
	# Camera
	camera.player = self
	camera.connect("sight_changed", get_parent(), "_sight_changed")
	
	# Prepare transform variables for other players
	if not is_network_master():
		last_transform = get_parent().transforms[int(name)]
		target_transform = last_transform


func _unhandled_key_input(event):
	if not event.is_echo():
		var is_pressed = event.is_pressed()
		if event.is_action("forward"): action.forward = is_pressed
		elif event.is_action("backward"): action.backward = is_pressed
		elif event.is_action("left"): action.left = is_pressed
		elif event.is_action("right"): action.right = is_pressed
		elif event.is_action("sprint"): action.sprint = is_pressed
		elif event.is_action("jump"): action.jump = is_pressed


func _physics_process(delta):
	var force_vector = Vector3(0.0, 0.0, 0.0)
	if is_network_master():
		if action.forward:
			force_vector += Vector3(sin(rotation.y), 0.0, cos(rotation.y))
		if action.backward:
			force_vector += -Vector3(sin(rotation.y), 0.0, cos(rotation.y))
		if action.left:
			force_vector += Vector3(cos(rotation.y), 0.0, -sin(rotation.y))
		if action.right:
			force_vector += -Vector3(cos(rotation.y), 0.0, -sin(rotation.y))

		if force_vector.length() != 0.0:
			force_vector = force_vector.normalized()
			if action.sprint:
				force_vector *= 2
			if not ground_contact:
				force_vector *= 0.5

			add_central_force(
				force_vector
				* force_amount
				* delta
				* (1/pow(linear_velocity.length() + 1, 1.5)
				)
			)

		if action.jump and ground_contact and jump_cooldown > 0.1:
			jump_cooldown = 0.0
			apply_central_impulse(Vector3.UP * 200)
			
		jump_cooldown += delta

	if not is_network_master():
		target_transform = get_parent().transforms[int(name)]
		if target_transform != last_transform:
			transform_interpolation_counter += delta
		else:
			transform_change_counter += delta

	if linear_velocity.length() > 1.0:
		$man/AnimationPlayer.play("walk")
	else:
		$man/AnimationPlayer.stop()

func _integrate_forces(_state):
	if is_network_master():
		rotation.y = target_rotation.y
		if transform != last_transform:
			emit_signal("transform_changed", transform)
		last_transform = transform
	else:
		if transform_change_counter != 0.0:
			var factor = transform_interpolation_counter / transform_change_counter
			transform = last_transform.interpolate_with(
				target_transform,
				clamp(factor, 0.0, 1.0)
			)
			if factor >= 1.0:
				last_transform = target_transform
				transform_interpolation_counter = 0
				transform_change_counter = 0
		else:
			transform = target_transform
			last_transform = target_transform
			transform_interpolation_counter = 0.0
			transform_change_counter = 0.0


func _on_Area_body_entered(body):
	if body == ground:
		ground_contact = true


func _on_Area_body_exited(body):
	if body == ground:
		ground_contact = false
