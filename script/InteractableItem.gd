extends RigidBody
class_name InteractableItem

export(bool) var interactable = true


func _ready():
	pass


func set_hud_visibility(visible):
	$HUD.visible = visible
