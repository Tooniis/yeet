extends InteractableItem
class_name GrabbableItem


func _ready():
	pass


func enable_physics():
	mode = MODE_RIGID
	set_collision_layer_bit(0, true)
	sleeping = false


func disable_physics():
	mode = MODE_CHARACTER
	set_collision_layer_bit(0, false)
	sleeping = true
