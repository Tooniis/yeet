shader_type spatial;

uniform vec2 wind;
uniform float horizontal_amplitude;
uniform float vertical_amplitude;
uniform float horizontal_frequency;
uniform float vertical_frequency;

uniform sampler2D albedo: hint_albedo;
uniform sampler2D roughness;
uniform sampler2D normalmap: hint_normal;
uniform sampler2D ao;
uniform sampler2D depth;
uniform float depth_scale;

void vertex() {
	// Base movement
	VERTEX.xz +=
		sin(TIME * horizontal_frequency)
		* wind
		* pow(VERTEX.y, 2.0)
		* 0.01 * horizontal_amplitude;
	
	// Branch movement
	VERTEX.y +=
		sin(TIME * vertical_frequency + VERTEX.z * 0.1 + sign(VERTEX.x))
		* pow(length(VERTEX.xz), 2.0)
		* 0.01 * vertical_amplitude;
}

void fragment() {
	ALBEDO = texture(albedo, UV).rgb;
	ROUGHNESS = texture(roughness, UV).g;
	NORMALMAP = texture(normalmap, UV).rgb;
	AO = texture(ao, UV).g;
	DEPTH = texture(depth, UV).g * depth_scale;
}