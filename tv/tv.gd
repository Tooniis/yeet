extends InteractableItem

const Channels = [
	[null, preload("res://audio/sound/noise.ogg"), false],
	[preload("res://video/rickroll.webm"), preload("res://audio/music/rickroll.ogg"), false],
	[preload("res://video/saul_3d.webm"), preload("res://audio/music/saul_3d.ogg"), true],
	[preload("res://video/wide_putin.webm"), preload("res://audio/music/song_for_denise.ogg"), false],
	[preload("res://video/20th_century_e.webm"), preload("res://audio/music/20th_century_e.ogg"), true],
	[preload("res://video/univers_e.webm"), preload("res://audio/music/univers_e.ogg"), true]
	
]

export(float, 0.0, 1.0) var button_travel
export(float) var button_toggle_duration
export(float) var knob_rotate_duration
export(float, 0.0, 1.0) var noise_channel_amount
export(AudioStream) var knob_sound
export(AudioStream) var button_sound

var knob_step = 360.0 / len(Channels)
onready var screen = $tv.get_surface_material(3)
onready var noise_amount = screen.get_shader_param("noise_amount")
onready var button_origin = $tv/button.translation

var power: bool setget set_power
var channel: int setget set_channel
var stream = {
	video = null,
	audio = null
}


func _ready():
	set_hud_visibility(false)
	set_power(false)
	self.channel = 0


func _input(event):
	if $HUD.visible: # if HUD is visible then player is in control
		if event.is_action_pressed("action_1"):
			request_power(not power)
		elif event.is_action_pressed("action_2"):
			request_channel(channel+1 if channel+1 < len(Channels) else 0)


func request_power(on: bool):
	rpc_id(1, "update_power", on)


func request_channel(id: int):
	rpc_id(1, "update_channel", id)


remote func set_power(on: bool):
	power = on
	$sound.stream = button_sound
	$sound.play()
	
	$Tween.interpolate_property(
		$tv/button, "translation:z",
		null,
		button_origin.z + button_travel/100 if on else button_origin.z,
		button_toggle_duration
	)
	$Tween.start()
	
	screen.set_shader_param("on", on)
	set_playback(on)


remote func set_channel(id: int):
	channel = id
	$sound.stream = knob_sound
	$sound.play()
	$Tween.interpolate_property(
		$tv/knob, "rotation_degrees:z",
		null,
		knob_step * id,
		knob_rotate_duration
	)
	$Tween.start()
	
	screen.set_shader_param("noise_amount", noise_channel_amount if id == 0 else noise_amount)
	set_playback(false)
	stream.video = Channels[id][0]
	stream.audio = Channels[id][1]
	set_playback(power)


remote func set_playback(playing: bool):
	if playing:
		$video.stream = stream.video
		$video.play()
		screen.set_shader_param("video", $video.get_video_texture())
		$audio.stream = stream.audio
		$audio.play()
	else:
		$video.stop()
		$video.stream = null
		$audio.stop()


# VideoPlayer's seeking mechanism not implemented yet
#remote func set_position(position: float):
#	$video.set_stream_position(position)
#	$audio.seek(position)


#remote func update_position():
#	rpc_unreliable_id(1, "set_position", $video.stream_position)


# Single player only
sync func update_power(on: bool):
	self.power = on


sync func update_channel(id: int):
	self.channel = id


func _on_video_finished():
	# Play again if video is meant to be played in a loop 
	if Channels[channel][2]:
		$video.play()
		$audio.play()
