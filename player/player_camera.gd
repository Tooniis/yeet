extends Camera

signal tilted
signal rotated

onready var target_fov = fov


func _input(event: InputEvent):
	if event is InputEventMouseMotion and not Main.platform_is_mobile \
	or event is InputEventScreenDrag and event.index != Main.joystick_touch_index:
		emit_signal("rotated", -deg2rad(event.relative.x) * Main.MOUSE_SENSITIVITY)
		emit_signal("tilted", -deg2rad(event.relative.y) * Main.MOUSE_SENSITIVITY)
	
	elif event is InputEventMouseButton and event.is_pressed():
		# Camera zoom
		if event.button_index == BUTTON_WHEEL_DOWN:
			if fov < Main.MAX_FOV:
				fov += Main.FOV_STEP
		elif event.button_index == BUTTON_WHEEL_UP:
			if fov > Main.MIN_FOV:
				fov -= Main.FOV_STEP
