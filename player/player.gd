extends KinematicBody

signal transform_changed

onready var ground := get_node("../land")
onready var camera := $armature/Skeleton/head/camera
onready var sight := $armature/Skeleton/head/camera/sight
onready var hands := [$armature/Skeleton/hand_left, $armature/Skeleton/hand_right]

export(float, 0.0, 1.0) var acceleration_factor = 0.4
export(float, 0.0, 1.0) var walk_factor = 0.5
export(float) var velocity_walk = 1.0
export(float) var maximum_velocity = 2.0
export(float) var velocity_sprint = 2.0
export(float) var velocity_jump = 3.0
export(float) var velocity_stop_threshold = 0.1

var velocity := Vector3.ZERO
var target_rotation := rotation
var ground_contact := false
var jump_cooldown := 0.0

var last_transform := Transform.IDENTITY
puppet var target_transform := last_transform
var transform_change_counter := 0.0
var transform_interpolation_counter := 0.0

var action := {
	jump = false
}

var current_interactable: Spatial = null
var last_interactable: Spatial = null


func _ready():
	# Connect scene to transform signal
	self.connect("transform_changed", get_parent(), "_player_transform_changed")
	
	# Prepare transform variables for other players
	if not is_network_master():
		last_transform = get_parent().transforms[int(name)]
		target_transform = last_transform


func _unhandled_input(event: InputEvent):
	if not event.is_echo() and event.is_action("jump"):
		action.jump = event.is_pressed()


func _physics_process(delta: float):
	if is_network_master():
		var movement := Vector3.ZERO
		
		# Primary movement
		movement += transform.basis.z * Input.get_action_strength("forward")
		movement += -transform.basis.z * Input.get_action_strength("backward")
		movement += transform.basis.x * Input.get_action_strength("left")
		movement += -transform.basis.x * Input.get_action_strength("right")
		
		# Scale unit vector to maximum velocity
		movement *= maximum_velocity
		
		# Add Secondary movements
		# Jump
		if action.jump and ground_contact and jump_cooldown > 0.1:
			jump_cooldown = 0.0
			movement += Vector3.UP * velocity_jump
			
			# Animate jump
			$AnimationTree.rpc("jump")
		
		# Add movement to velocity
		velocity.x = lerp(velocity.x, movement.x, acceleration_factor)
		velocity.z = lerp(velocity.z, movement.z, acceleration_factor)
		velocity.y += movement.y
		
		# Add gravity to velocity
		velocity += Vector3.DOWN * ground.GRAVITY * delta
		
		# Remove errors
		if Vector2(velocity.x, velocity.z).length() < velocity_stop_threshold:
			velocity.x = 0
			velocity.z = 0
		
		# Move
		velocity = move_and_slide(velocity, Vector3.UP, true)
		
		# Report transform (multiplayer)
		if transform != last_transform:
			emit_signal("transform_changed", transform)
			if not Main.offline:
				#rpc_unreliable_id(1, "update_transform", transform)
				rset_unreliable("target_transform", transform)
		last_transform = transform
		
			
		# Main Animation
		var direction = velocity.rotated(Vector3.UP, -rotation.y)
		direction = Vector2(-direction.x, direction.z) / velocity_walk
		if ground_contact:
			$AnimationTree.rpc_unreliable("set_direction", direction)
			$AnimationTree.rpc_unreliable("set_speed", direction.length())
		
		# Count
			jump_cooldown += delta
	
	else:
		# Get newest transform
		target_transform = get_parent().transforms[int(name)]
		
		if target_transform != last_transform:
			transform_interpolation_counter += delta
		else:
			transform_change_counter += delta
		
		if transform_change_counter != 0.0:
			var factor = transform_interpolation_counter / transform_change_counter
			transform = last_transform.interpolate_with(
				target_transform,
				clamp(factor, 0.0, 1.0)
			)
			if factor >= 1.0:
				last_transform = target_transform
				transform_interpolation_counter = 0
				transform_change_counter = 0
		else:
			transform = target_transform
			last_transform = target_transform
			transform_interpolation_counter = 0.0
			transform_change_counter = 0.0


func _on_camera_rotated(phi: float):
	rotation.y += phi


func _on_Area_body_entered(body):
	if body == ground:
		ground_contact = true
		
		# Animate landing
		if not action.jump:
			$AnimationTree.land()


func _on_Area_body_exited(body):
	if body == ground:
		ground_contact = false


func _on_Skeleton_head_pose_changed(new_pose):
	if is_network_master():
		rpc_unreliable("set_head_pose", new_pose)


puppet func set_head_pose(new_pose):
	$armature/Skeleton.set_bone_custom_pose($armature/Skeleton.HeadBone, new_pose)


func _input(event):
	if is_network_master() and event is InputEventMouseButton and event.is_pressed():
		var hand_index = 0 if event.button_index == BUTTON_LEFT else 1 if event.button_index == BUTTON_RIGHT else null
		if hand_index != null:
			if hands[hand_index].get_item() == null:
				if sight.item is GrabbableItem:
					hands[hand_index].rpc("grab", sight.item.get_path())
			#elif interactable.object != null and interactable.object.can_recieve(hands[0].get_object()):
			elif hands[hand_index].action == Hand.actions.INSERT:
				hands[hand_index].send(sight.item)
			else:
				hands[hand_index].rpc("release")


func update_hand_actions():
	for hand in hands:
		if sight.item != null and sight.item.has_method("can_recieve") and sight.item.can_recieve(hand.get_item()):
			hand.set_action(Hand.actions.INSERT)
		else:
			hand.set_action(Hand.actions.RELEASE)


func _on_interactable_ray_item_changed():
	# Connect interactable's recieving signal if applicable
	if sight.item != null and sight.item.has_signal("recieving_changed"):
		sight.item.connect("recieving_changed", self, "_on_interactable_recieving_changed"
		)
	
	# Disconnect last interactable's recieving signal if applicable
	if sight.last_item != null and sight.last_item.has_signal("recieving_changed"):
		sight.last_item.disconnect("recieving_changed", self, "_on_interactable_recieving_changed")
	
	update_hand_actions()


func _on_interactable_recieving_changed(_is_recieving):
	update_hand_actions()


func _on_hand_left_item_changed():
	update_hand_actions()


func _on_hand_right_item_changed():
	update_hand_actions()
