extends RayCast

signal item_changed

var item: InteractableItem
var grabbable_item: GrabbableItem
var last_item: InteractableItem


func _physics_process(_delta):
	# Keep record of the last item
	last_item = item
	
	# Get new item
	item = get_collider()
	if not item is InteractableItem:
		item = null
	
	if item != last_item:
		emit_signal("item_changed")
		
		# Manage HUDs
		if item != null:
			item.set_hud_visibility(true)
		if last_item != null:
			last_item.set_hud_visibility(false)
	
	# Get grabbable item
	grabbable_item = item if item is GrabbableItem else null
