extends AnimationTree

const WALK_SPACE = "parameters/walk/blend_position"
const SPRINT_SPACE = "parameters/sprint/blend_position"
const MIXER = "parameters/mixer/blend_amount"
const SPEED = "parameters/speed/scale"
const STATE = "parameters/state/current"
const LAND_ACTIVE = "parameters/land_oneshot/active"
const CATCH_LEFT = "parameters/catch_left/blend_amount"
const CATCH_RIGHT = "parameters/catch_right/blend_amount"
const CATCH_MODE_LEFT = "parameters/catch_mode_right/blend_amount"
const CATCH_MODE_RIGHT = "parameters/catch_mode_right/blend_amount"

enum states {
	MAIN = 0,
	JUMP = 1
}

export var idle_threshold = 0.1
export(float, 0.0, 1.0) var catch_duration
export(float, 0.0, 1.0) var release_duration


sync func set_direction(direction: Vector2):
	if direction.length() < idle_threshold:
		direction = Vector2.ZERO
	self[WALK_SPACE] = direction
	self[SPRINT_SPACE] = direction


sync func set_speed(speed: float):
	if speed >= 1.0:
		self[SPEED] = speed
		self[MIXER] = clamp(speed - 1.0, 0.0, 1.0)


sync func jump():
	self[LAND_ACTIVE] = false
	if self[STATE] != states.JUMP:
		self[STATE] = states.JUMP


func land():
	self[LAND_ACTIVE] = true
	self[STATE] = states.MAIN


func _on_hand_left_grabbed():
	$Tween.interpolate_property(self, CATCH_LEFT, null, 0.9, catch_duration)
	$Tween.start()


func _on_hand_right_grabbed():
	$Tween.interpolate_property(self, CATCH_RIGHT, null, 0.9, catch_duration)
	$Tween.start()


func _on_hand_left_released():
	$Tween.interpolate_property(self, CATCH_LEFT, null, 0.0, release_duration)
	$Tween.start()


func _on_hand_right_released():
	$Tween.interpolate_property(self, CATCH_RIGHT, null, 0.0, release_duration)
	$Tween.start()
