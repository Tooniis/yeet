extends Skeleton

signal head_pose_changed

const HEAD_MIN_TILT = deg2rad(-75)
const HEAD_MAX_TILT = deg2rad(85)

onready var HeadBone = find_bone("head")
onready var ChestBone := find_bone("chest")

export(float, 0.0, 1.0) var raise_hand_duration
export(float, 0.0, 1.0) var lower_hand_duration


func _ready():
	$shoulder_axis.transform.basis = get_bone_global_pose(HeadBone).basis


func _on_camera_tilted(phi: float):
	# Limit rotation by clamping absolute rotation then calculating the resulting difference
	var phi_limited = clamp(
		phi + get_bone_custom_pose(HeadBone).basis.get_euler().x,
		HEAD_MIN_TILT,
		HEAD_MAX_TILT
	) - get_bone_custom_pose(HeadBone).basis.get_euler().x

	var direction = get_bone_custom_pose(HeadBone) * Basis.IDENTITY.y
	var normal = get_bone_custom_pose(HeadBone) * Basis.IDENTITY.z
	var tilt_axis = direction.cross(normal).normalized()

	# Rotate head
	set_bone_custom_pose(HeadBone, get_bone_custom_pose(HeadBone).rotated(tilt_axis, phi_limited))
	emit_signal("head_pose_changed", get_bone_custom_pose(HeadBone))
	
	# Correct floating-point errors
	set_bone_custom_pose(HeadBone, get_bone_custom_pose(HeadBone).orthonormalized())

	$shoulder_axis.transform.basis = get_bone_global_pose(HeadBone).basis

func _on_hand_left_grabbed():
	$hand_left_ik.start()
	$Tween.stop_all()
	$Tween.reset_all()
	$Tween.interpolate_property($hand_left_ik, "interpolation", 0.0, 1.0, raise_hand_duration)
	$Tween.start()


func _on_hand_right_grabbed():
	$hand_right_ik.start()
	$Tween.stop_all()
	$Tween.reset_all()
	$Tween.interpolate_property($hand_right_ik, "interpolation", 0.0, 1.0, raise_hand_duration)
	$Tween.start()


func _on_hand_left_released():
	$Tween.interpolate_property($hand_left_ik, "interpolation", 1.0, 0.0, lower_hand_duration)
	$Tween.interpolate_callback($hand_left_ik, lower_hand_duration, "stop")
	$Tween.interpolate_callback(self, lower_hand_duration, "clear_bones_global_pose_override")
	$Tween.start()


func _on_hand_right_released():
	$Tween.interpolate_property($hand_right_ik, "interpolation", 1.0, 0.0, lower_hand_duration)
	$Tween.interpolate_callback($hand_right_ik, lower_hand_duration, "stop")
	$Tween.interpolate_callback(self, lower_hand_duration, "clear_bones_global_pose_override")
	$Tween.start()
