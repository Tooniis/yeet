extends BoneAttachment
class_name Hand

signal grabbed
signal released
signal item_changed

enum actions {
	RELEASE = 0,
	INSERT = 1
}

var action: int = actions.RELEASE
var transforms = {}
var child_count := 0


func _ready():
	connect("grabbed", self, "_on_grab")
	connect("released", self, "_on_release")
	
	for node in get_children():
		if node is Position3D:
			transforms[node.name] = node.transform
			node.free()

	child_count = get_child_count()
	
	set_action(actions.RELEASE)


func get_item() -> GrabbableItem:
	var item: GrabbableItem = get_child(0) if get_child_count() != child_count else null
	return item


func get_hold_transform(object: Spatial) -> Transform:
	return get_node("transform_" + object.TYPE).transform


func request_grab(object: Spatial):
	rpc_id(1, "grab", object)


sync func grab(path: NodePath):
	var item: GrabbableItem = get_node(path)
	# Grab only when hand is empty
	if get_item() == null:
		# Call release_item() is last parent was a special item
		if item.holder != item.original_parent:
			item.holder.release_item(item)
		
		# Remove object from parent
		item.get_parent().remove_child(item)
		
		# Grab item
		add_child(item)
		item.holder = self
		
		# Disable interaction with item
		item.interactable = false
		
		# Make item child 0
		move_child(item, 0)
		
		item.disable_physics()
		item.transform = transforms[item.TYPE]
		
		emit_signal("grabbed")


func set_action(new_action: int):
	if new_action == actions.INSERT:
		$hud/Viewport/Control/CenterContainer/HBoxContainer/action.text = "Insert"
	elif new_action == actions.RELEASE:
		$hud/Viewport/Control/CenterContainer/HBoxContainer/action.text = "Release"		
	action = new_action


sync func release():
	var released_item := get_item()
	released_item.transform = released_item.get_global_transform()
	remove_child(released_item)

	released_item.original_parent.add_child(released_item)
	released_item.holder = released_item.original_parent
	released_item.enable_physics()
	released_item.interactable = true
	emit_signal("released")


func send(destination: Spatial):
	var released_item := get_item()
	released_item.interactable = true
	remove_child(released_item)
	released_item.transform = Transform.IDENTITY
	destination.add_child(released_item)
	released_item.holder = destination
	destination.receive_item(released_item)
	emit_signal("released")


func _on_grab():
	# Show HUD
	$hud.visible = true
	emit_signal("item_changed")


func _on_release():
	# Hide HUD
	$hud.visible = false
	emit_signal("item_changed")
