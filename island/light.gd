extends DirectionalLight

signal shadows_init


func _ready():
	emit_signal("shadows_init", shadow_enabled)


func _on_settings_shadows_toggled(enabled):
	shadow_enabled = enabled
