extends Control

signal toggled

enum modes {
	OFF = 0,
	TITLE = 1,
	SETTINGS = 2,
	PAUSE = 3
}

var mode: int = modes.OFF
var previous_mode: int = modes.OFF

export(float, 1, 179) var fov = 70
export(float) var ratio = 1


func set_mode(new_mode: int):
	for node in get_children():
		if node is Control:
			set_enabled(node, false)
	
	if new_mode == modes.TITLE:
		# Show title screen
		set_enabled($title, true)
	
	elif new_mode == modes.SETTINGS:
		# Show settings screen
		set_enabled($settings, true)
	
	elif new_mode == modes.PAUSE:
		# Show pause menu
		set_enabled($pause, true)
	
	elif new_mode == modes.OFF:
		$HUD.visible = true
	
	previous_mode = mode
	mode = new_mode
	
	emit_signal("toggled", not mode == modes.OFF)


func set_enabled(node: Control, enabled: bool):
	node.visible = enabled
	node.set_process_input(enabled)


func _on_settings_requested():
	set_mode(modes.SETTINGS)


func _on_settings_quit():
	set_mode(previous_mode)


func _on_pause_quit():
	set_mode(modes.OFF)


func _on_pause_title_requested():
	pass
