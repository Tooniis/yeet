extends InteractableItem

signal door_opened
signal door_closed
signal stopped
signal played
signal recieving_changed

const BUTTON_TRAVEL = 0.01
const BUTTON_PRESS_DURATION = 0.2
const DOOR_OPEN_ROTATION_DEGREES = 30.0
const PITCH_FAST = 20.0
const MAX_POSITION_DIFFERENCE = 1.0
const CASSETTE_TRANSLATION = Vector3(0.0, 0.063, -0.056)
const CASSETTE_ROTATION_DEGREES = Vector3(90.0, -180.0, 0.0)

onready var button_stop_origin = $stop.translation
onready var button_fastforward_origin = $fastforward.translation
onready var button_play_origin = $play.translation
onready var button_rewind_origin = $rewind.translation
onready var button_pause_origin = $pause.translation
onready var door_rotation_origin = $door.rotation

export(String) var recieve_type
export(float, 0.05, 0.5) var door_open_duration
export(float, 0.05, 0.5) var door_close_duration

var stop_pressed := false
var button_pressed_timer := 0.0
var action_5 := "stop"

# Offline mode only
var is_paused := false
var playback_mode := "stop"
remote var is_door_closed := true
var is_recieving := false


func _ready():
	mode = MODE_RIGID

func _process(delta):
	if stop_pressed:
		button_pressed_timer -= delta
		if button_pressed_timer <= 0.0:
			$stop.translation = button_stop_origin
			stop_pressed = false


func _input(event):
	if $HUD.visible: # if HUD is visible then player is in control
		var action = ""
		if event.is_action_pressed("action_1"):
			action = "pause"
		elif event.is_action_pressed("action_2"):
			action = "rewind"
		elif event.is_action_pressed("action_3"):
			action = "play"
		elif event.is_action_pressed("action_4"):
			action = "fastforward"
		elif event.is_action_pressed("action_5"):
			action = action_5
		if action != "":
			rpc_id(1, "update_boombox", action)


remote func update_pause(paused):
	if paused:
		$audio.stream_paused = true
		$pause.translation.y = button_pause_origin.y - BUTTON_TRAVEL
	else:
		$audio.stream_paused = false
		$pause.translation = button_pause_origin


remote func update_playback_mode(mode):
	call(mode)


remote func update_door(closed):
	is_door_closed = closed
	
	if closed:
		# Close door
		$door/Tween.interpolate_property(
			$door, "rotation",
			null, door_rotation_origin,
			door_close_duration
		)
		$door/Tween.start()
		$sound.stream = $sound.close_door
		$sound.play()
		
		emit_signal("door_closed")
		set_recieving(false)
		action_5 = "stop"
	else:
		# Open door
		$door/Tween.interpolate_property(
			$door, "rotation:x",
			null, door_rotation_origin.x - deg2rad(DOOR_OPEN_ROTATION_DEGREES),
			door_open_duration
		)
		$door/Tween.start()
		
		emit_signal("door_opened")
		
		# Accept cassettes only if empty
		if $audio.stream == null:
			set_recieving(true)
		
		action_5 = "close_door"


remote func send_position():
	rpc_id(1, "update_position", $audio.get_playback_position())


remote func sync_audio(position):
	# Avoid unneeded sync if current position is close enough
	if abs($audio.get_playback_position() - position) > MAX_POSITION_DIFFERENCE:
		$audio.seek(position)


func stop():
	# Press button
	$stop.translation.y = button_stop_origin.y - BUTTON_TRAVEL
	button_pressed_timer = BUTTON_PRESS_DURATION
	stop_pressed = true
	$sound.stream = $sound.stop_button
	$sound.play()
	
	# Reset playback buttons
	$play.translation = button_play_origin
	$fastforward.translation = button_fastforward_origin
	$rewind.translation = button_rewind_origin

	$audio.playing = false
	if is_door_closed:
		emit_signal("stopped")
		action_5 = "stop"


func fastforward():
	# Press button
	$fastforward.translation.y = button_fastforward_origin.y - BUTTON_TRAVEL
	$sound.stream = $sound.playback_button
	$sound.play()

	# Reset other playback buttons
	$play.translation = button_play_origin
	$rewind.translation = button_rewind_origin

	# Set pitch
	$audio.pitch_scale = PITCH_FAST
	
	# Play if not already playing
	if not $audio.playing:
		$audio.playing = true
		emit_signal("played")
		action_5 = "stop"


func play():
	# Press button
	$play.translation.y = button_play_origin.y - BUTTON_TRAVEL
	$sound.stream = $sound.playback_button
	$sound.play()

	# Reset other playback buttons
	$fastforward.translation = button_fastforward_origin
	$rewind.translation = button_rewind_origin
	
	# Set pitch
	$audio.pitch_scale = 1.0
	
	# Play if not already playing
	if not $audio.playing:
		$audio.playing = true
		emit_signal("played")
		action_5 = "stop"


func rewind():
	pass


# Offline mode only
sync func update_boombox(action):
	# Toggle pause
	if action == "pause":
		is_paused = not is_paused
		update_pause(is_paused)
	
	# Close door if not closed
	elif action == "close_door":
		if not is_door_closed:
			is_door_closed = true
			update_door(is_door_closed)

	# Open door if playback is already stopped and door is closed
	elif action == "stop" and playback_mode == "stop":
		if is_door_closed:
			is_door_closed = false
			update_door(is_door_closed)
			# Update playback mode to trigger button animation
			update_playback_mode("stop")

	# Update playback mode if current mode is different than button pressed, and only if door is closed
	elif action != playback_mode:
		if is_door_closed:
			playback_mode = action
			update_playback_mode(playback_mode)


func set_recieving(recieving):
	is_recieving = recieving
	emit_signal("recieving_changed", is_recieving)


func can_recieve(item: Node) -> bool:
	return true if is_recieving and item != null and item.TYPE == recieve_type else false


func receive_item(item: Spatial):
	# Receive cassette
	add_collision_exception_with(item)
	item.add_collision_exception_with(self)
	remove_child(item)
	$door.add_child(item)
	$sound.stream = $sound.insert_cassette
	$sound.play()
	
	# Stop receiving
	set_recieving(false)
	
	# Set cassette transform
	item.transform = $door/transform_cassette.transform
	
	# Load audio stream
	$audio.stream = item.stream


func release_item(item: Spatial):
	remove_collision_exception_with(item)
	item.remove_collision_exception_with(self)
	
	# Start receiving
	set_recieving(true)
	
	# Unload audio stream
	$audio.stream = null


func set_hud_visibility(visible):
	.set_hud_visibility(visible)
	$HUD.visible = visible
