extends Control


func set_playback_element_visibility(visible: bool):
	$CenterContainer/VSplitContainer/GridContainer/button_1.visible = visible
	$CenterContainer/VSplitContainer/GridContainer/button_2.visible = visible
	$CenterContainer/VSplitContainer/GridContainer/button_3.visible = visible
	$CenterContainer/VSplitContainer/GridContainer/button_4.visible = visible
	$CenterContainer/VSplitContainer/GridContainer/pause.visible = visible
	$CenterContainer/VSplitContainer/GridContainer/rewind.visible = visible
	$CenterContainer/VSplitContainer/GridContainer/play.visible = visible
	$CenterContainer/VSplitContainer/GridContainer/fastforward.visible = visible


func _on_boombox_played():
	$CenterContainer/VSplitContainer/GridContainer/stop.visible = true
	$CenterContainer/VSplitContainer/GridContainer/open.visible = false
	$CenterContainer/VSplitContainer/GridContainer/close.visible = false


func _on_boombox_stopped():
	$CenterContainer/VSplitContainer/GridContainer/stop.visible = false
	$CenterContainer/VSplitContainer/GridContainer/open.visible = true
	$CenterContainer/VSplitContainer/GridContainer/close.visible = false


func _on_boombox_door_opened():
	set_playback_element_visibility(false)
	
	$CenterContainer/VSplitContainer/GridContainer/stop.visible = false
	$CenterContainer/VSplitContainer/GridContainer/open.visible = false
	$CenterContainer/VSplitContainer/GridContainer/close.visible = true


func _on_boombox_door_closed():
	set_playback_element_visibility(true)
	_on_boombox_stopped()
