extends Spatial

const INTERACTABLE_BIT = 5

const PlayerScene = preload("res://player/player.tscn")
const SettingsPanel = preload("res://ui/settings.tscn")

remote var transforms = {}

onready var active_camera: Camera = $Camera


func _ready():
	$interface.set_mode($interface.modes.TITLE)
	$Camera.make_current()
	
	# Connect Main to title screen signals
	$interface/title.get_node("client/ip_dialog/ip_box").connect("ip_obtained", Main, "_ip_obtained")
	$interface/title.get_node("name/name_box").connect("text_entered", Main, "_name_obtained")
	$interface/title.get_node("buttons/offline").connect("pressed", Main, "_offline_mode_selected")
	
	Main.connect("ready_to_start", self, "_ready_to_start")
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
	Main.connect("info_available", self, "_info_available")
	Main.connect("player_spawned", self, "_player_spawned")


func _process(_delta):
	Main.camera_translation = active_camera.translation + active_camera.get_parent().translation


func _input(event):
	if event.is_action_pressed("quit"):
		if $interface.mode == $interface.modes.OFF:
			# Show pause menu
			$interface.set_mode($interface.modes.PAUSE)


func spawn_player(id):
	var player = PlayerScene.instance()
	
	player.set_name(str(id))

	if not Main.offline:
		# Make player master on himself
		player.set_network_master(id)
		# Set name tag
		player.get_node("Viewport/Label").text = Main.infos[id].name
	
	else:
		# Remove my name tag
		player.remove_child(player.get_node("name_tag"))
	
	add_child(player)
	
	if id != Main.get_id():
		# Remove camera from dummy player
		player.camera.queue_free()
	
	# Translate to player spawn point
	player.translate(Main.infos[id].spawn)
	
	return player


func _ready_to_start():
	print("ready to start")
	
	# Disable quit by back button for mobile
	get_tree().set_auto_accept_quit(false)
	
	# Remove chat if offline
	if Main.offline:
		$interface/HUD/chat.queue_free()
	
	# Spawn existing players
	for id in Main.infos:
		print(Main.infos[id])
		var player = spawn_player(id)
		if id == Main.get_id():
			# Remove my name tag
			player.remove_child(player.get_node("name_tag"))
			
			# Activate player camera if it is me
			player.camera.make_current()
			active_camera = player.camera
			# Hide title screen
			$interface.set_mode($interface.modes.OFF)
			# Make HUD visible
			$interface/HUD.visible = true
	
	if not Main.offline:
		print("network id ", Main.get_id())


func _player_spawned(id):
	print("scene, player registered")
	spawn_player(id)


func _player_disconnected(id):
	get_node(str(id)).queue_free()


func _info_available(message):
	$interface/HUD/info.text = message


func _player_transform_changed(new_transform):
	if not Main.offline:
		rpc_unreliable_id(1, "update_player_transform", new_transform)


func _on_interface_toggled(enabled):
	set_process_input(not enabled)
	active_camera.set_process_input(not enabled)
	
	if enabled:
		# Release mouse
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		
		# Hide joystick
		$mobile_control.visible = false
	else:
		if Main.platform_is_mobile:
			# Show mobile controls if mobile
			$mobile_control.visible = true
		else:
		# Capture mouse
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

