shader_type spatial;
render_mode specular_blinn, cull_disabled, depth_draw_opaque, shadows_disabled;

uniform sampler2D noise;
uniform sampler2D normalmap;
uniform vec3 tint;
uniform float alpha;
uniform float rim;
uniform float texture_scale;
uniform float height_scale;
varying vec3 pos;
varying vec2 offset;

float wave(vec2 position) {
	position += texture(noise, position).x * 2.0 - 1.0;
	vec2 wave_vector = 1.0 - abs(sin(position));
	return pow(1.0 - pow(wave_vector.x * wave_vector.y, 0.65), 4.0);
}

void vertex() {
	pos = VERTEX.xyz / texture_scale;
	offset = 0.01 * cos(pos.xz + TIME) + TIME * 0.02;
	float wave_amount = wave((pos.xz - offset) * 2.0)
	+ wave((pos.xz + offset) * 3.0)
	+ wave((pos.xz - offset) - 2.0);
	//NORMAL = normalize(vec3(k - height(pos + vec2(0.1, 0.0), texture_scale, TIME), 0.3, k - height(pos + vec2(0.0, 0.1), texture_scale, TIME)));
}

void fragment() {
	float fresnel = sqrt(1.0 - dot(NORMAL, VIEW) / texture_scale);
	
	ALBEDO = tint + (1.0 - fresnel);
	METALLIC = 0.0;
	ROUGHNESS = 0.01 * (1.0 - fresnel);
	RIM = rim;
	vec3 normal = (texture(normalmap, (pos.xz - offset)).xyz + texture(normalmap, (pos.xz + offset)).xyz) / 2.0;
//	float angle = 0.5 * sin(TIME + texture((noise), pos.xz).x) / (VERTEX.z);
//	mat3 rotation = mat3(
//		vec3(cos(angle), -sin(angle), 0.0),
//		vec3(sin(angle), cos(angle), 0.0),
//		vec3(0.0, 0.0, 1.0)
//	);
//	NORMALMAP = rotation * normal;
	NORMALMAP = normal;
	TRANSMISSION = vec3(1.0, 1.0, 1.0);
	ALPHA = alpha;
}