extends GrabbableItem

const TYPE = "cassette"

onready var original_transform = transform
onready var original_translation = translation
onready var original_parent = get_parent()
onready var holder = original_parent

export(AudioStream) var stream
export(Transform) var held_transform
export(float, 0.01, 1) var minimum_margin
export(float, 0.01, 1) var maximum_margin
export(float) var velocity_margin_factor
export(float) var respawn_distance

var destination: Node = null


func _physics_process(_delta):
	if mode == MODE_CHARACTER:
		linear_velocity = Vector3.ZERO
	
	if translation.distance_to(original_translation) > respawn_distance and holder == original_parent:
		transform = original_transform
		linear_velocity = Vector3.ZERO
		print(translation.distance_to(original_transform.xform(Vector3.ONE)))


func _integrate_forces(_state):
	# Increase margin at high velocity to prevent going through bodies
	if linear_velocity.length() > 1:
		$CollisionShape.shape.margin = stepify(clamp(
			linear_velocity.length() * velocity_margin_factor,
			minimum_margin,
			maximum_margin
		), 0.01)
	elif abs($CollisionShape.shape.margin - minimum_margin) > 0.001:
		$CollisionShape.shape.margin = minimum_margin


func set_hud_visibility(visible):
	.set_hud_visibility(visible)
	$HUD.visible = visible


#func _interactable_changed(object):
#	$HUD.visible = true if object == self else false


func _held_objects_changed(left_object, right_object):
	if left_object != null:
		$Viewport/Control/CenterContainer/VSplitContainer/HBoxContainer/grab_left.visible = false
		$Viewport/Control/CenterContainer/VSplitContainer/HBoxContainer/mouse_left.visible = false
	else:
		$Viewport/Control/CenterContainer/VSplitContainer/HBoxContainer/grab_left.visible = true
		$Viewport/Control/CenterContainer/VSplitContainer/HBoxContainer/mouse_left.visible = true
	
	if right_object != null:
		$Viewport/Control/CenterContainer/VSplitContainer/HBoxContainer/grab_right.visible = false
		$Viewport/Control/CenterContainer/VSplitContainer/HBoxContainer/mouse_right.visible = false
	else:
		$Viewport/Control/CenterContainer/VSplitContainer/HBoxContainer/grab_right.visible = true
		$Viewport/Control/CenterContainer/VSplitContainer/HBoxContainer/mouse_right.visible = true

#
#sync func set_master(id):
#	set_network_master(id)
