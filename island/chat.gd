extends VBoxContainer


func _input(event):
	if event.is_action_pressed("ui_chat"):
		$message_box.visible = true
		$message_box.grab_focus()
	if event.is_action_released("ui_chat"):
		$message_box.editable = true

func _on_message_box_text_entered(text):
	rpc_id(1, "send_message", text)
	$message_box.text = ""
	$message_box.release_focus()
	$message_box.visible = false
	$message_box.editable = false


remote func get_message(text, id):
	if id == 1:
		$history.text += "\n" + text
	else:
		$history.text += "\n" + "[" + Main.infos[id].name + "] " + text
