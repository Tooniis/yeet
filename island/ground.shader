shader_type spatial;

// Textures
uniform sampler2D albedo_1: hint_albedo;
uniform sampler2D roughness_1;
uniform sampler2D ao_1;
uniform sampler2D normalmap_1: hint_normal;
uniform sampler2D albedo_2: hint_albedo;
uniform sampler2D roughness_2;
uniform sampler2D ao_2;
uniform sampler2D normalmap_2: hint_normal;
uniform sampler2D tint_map;
uniform sampler2D noise;

// Parameters
uniform float bias;
uniform float transition;
uniform float wet_bias;
uniform float wet_transition;
uniform float wet_max;
uniform float noise_size;
uniform float angle_variance;
uniform float normal_bump;
uniform float roughness: hint_range(0.0, 2.0);
uniform vec3 dirt_tint;

varying vec3 vertex_position;

vec3 saturate(vec3 color, float saturation) {
	float average = (color.r + color.g + color.b) / 3.0;
	return mix(color, vec3(average), saturation);
}

vec4 texture_mixed(sampler2D texture_1, sampler2D texture_2, vec2 uv, float factor) {
	return mix(texture(texture_1, uv), texture(texture_2, uv), factor);
}

vec2 rotate2D(vec2 vector, float angle) {
	return mat2(
		vec2(cos(angle), -sin(angle)),
		vec2(sin(angle), cos(angle))
	) * vector;
}

void vertex() {
	vertex_position = VERTEX;
}

void fragment() {
	float factor = clamp((vertex_position.y + bias) / transition, 0.0, 1.0);
	float wet = 1.0 - pow(clamp((vertex_position.y + wet_bias) / wet_transition, -wet_max, 0.0), 2.0);
	float angle = texture(noise, (ceil(UV) / noise_size) / 2.0).x * angle_variance;
	vec2 uv = rotate2D(UV - floor(UV) - 0.5, angle) + 0.5;
	
	float saturation = 1.0 - texture(tint_map, UV2).r;
	vec3 tint = mix(vec3(1.0), dirt_tint, saturation);
	//vec2 uv_albedo = (uv - 1.0) * texture(noise, (ceil(UV) / noise_size) / 2.0).y;
	ALBEDO = mix(texture(albedo_1, uv).rgb, saturate(texture(albedo_2, uv).rgb, saturation) * tint, factor).rgb * wet;
	//ALBEDO = saturate(texture_mixed(albedo_1, albedo_2, (uv - 1.0) * texture(noise, (ceil(UV) / noise_size) / 2.0).y, factor).rgb * wet, sat);
	ROUGHNESS = texture_mixed(roughness_1, roughness_2, uv, factor).r * wet * roughness;
	AO = texture_mixed(ao_1, ao_2, uv, factor).r;
	NORMALMAP = texture_mixed(normalmap_1, normalmap_2, uv, factor).rgb * wet * normal_bump;
	SPECULAR = 0.0;
}