extends Control

signal settings_requested
signal title_requested
signal quit


func _on_return_pressed():
	emit_signal("quit")


func _on_settings_pressed():
	emit_signal("settings_requested")


func _on_title_pressed():
	emit_signal("title_requested")


func _input(event):
	if event.is_action_pressed("quit"):
		emit_signal("quit")


func _on_quit_pressed():
	get_tree().quit()
