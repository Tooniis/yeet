extends Button

export(String) var action
var event := InputEventAction.new()


func _ready():
	event.action = action


func _on_button_down():
	event.pressed = true
	Input.parse_input_event(event)


func _on_button_up():
	event.pressed = false
	Input.parse_input_event(event)
