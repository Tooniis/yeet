extends Node2D

export(float, 0.0, 1.0) var deadzone_radius

var started_in_range := false
var vector := Vector2.ZERO

var forward_action := InputEventAction.new()
var backward_action := InputEventAction.new()
var left_action := InputEventAction.new()
var right_action := InputEventAction.new()
var horizontal_event := InputEventAction.new()
var vertical_event := InputEventAction.new()


func _init():
	forward_action.action = "forward"
	backward_action.action = "backward"
	left_action.action = "left"
	right_action.action = "right"


func move_stick(position: Vector2):
	position -= $range.position
	if position.length() > $range/Area2D/CollisionShape2D.shape.radius:
		position = position.normalized() * $range/Area2D/CollisionShape2D.shape.radius
	$range/stick.position = position
	
	vector = $range/stick.position * Vector2(1.0, -1.0) / $range/Area2D/CollisionShape2D.shape.radius
	if vector.length() < deadzone_radius:
		vector = Vector2.ZERO
	
	Main.parse_movement_events(vector)


func _input(event: InputEvent):
	if event is InputEventScreenDrag and event.index == Main.joystick_touch_index and started_in_range:
		move_stick(event.position)
	elif event is InputEventScreenTouch and event.index == Main.joystick_touch_index and not event.is_pressed():
		started_in_range = false
		Main.joystick_touch_index = -1
		move_stick($range.position)


func _on_range_input_event(_viewport: Viewport, event: InputEvent, _shape_idx: int):
	if event is InputEventScreenTouch and event.is_pressed():
		started_in_range = true
		Main.joystick_touch_index = event.index
		move_stick(event.position)
