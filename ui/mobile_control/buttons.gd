extends Control

var action_1 := InputEventAction.new()
var action_2 := InputEventAction.new()
var jump := InputEventAction.new()


func _init():
	action_1.action = "action_1"
	action_2.action = "action_2"
	jump.action = "jump"


func _on_action_1_down():
	action_1.pressed = true
	Input.parse_input_event(action_1)


func _on_action_1_up():
	action_1.pressed = false
	Input.parse_input_event(action_1)


func _on_action_2_down():
	action_2.pressed = true
	Input.parse_input_event(action_2)


func _on_action_2_up():
	action_2.pressed = false
	Input.parse_input_event(action_2)


func _on_jump_down():
	jump.pressed = true
	Input.parse_input_event(jump)


func _on_jump_up():
	jump.pressed = false
	Input.parse_input_event(jump)
