extends Control

signal quit
signal shadows_toggled

const VSYNC = "display/window/vsync/use_vsync"
const MSAA = "rendering/quality/filters/msaa"
const AF = "rendering/quality/filters/anisotropic_filter_level"
const SHADOW = "rendering/quality/directional_shadow/size"


func _ready():
	$MarginContainer/Panel/VBoxContainer/fullscreen/state.pressed = OS.window_fullscreen
	$MarginContainer/Panel/VBoxContainer/vsync/state.pressed = int(ProjectSettings.get_setting(VSYNC))
	$MarginContainer/Panel/VBoxContainer/quality/aa.value = log2(ProjectSettings.get_setting(MSAA))
	_on_aa_value_changed($MarginContainer/Panel/VBoxContainer/quality/aa.value)
	#$MarginContainer/Panel/VBoxContainer/quality/af.value = log2(ProjectSettings.get_setting(AF))
	#_on_af_value_changed($MarginContainer/Panel/VBoxContainer/quality/af.value)


func _on_DirectionalLight_shadows_init(enabled):
	$MarginContainer/Panel/VBoxContainer/shadows/state.pressed = enabled


func _on_fullscreen_toggled(button_pressed):
	OS.window_fullscreen = button_pressed


func _on_vsync_toggled(button_pressed):
	OS.vsync_enabled = button_pressed
	print(OS.vsync_enabled)


func _on_aa_value_changed(value):
	$MarginContainer/Panel/VBoxContainer/quality/aa_value.text = "Off" if not value else str(int(pow(2.0, value)))
	get_viewport().msaa = value


func _on_af_value_changed(value):
	$MarginContainer/Panel/VBoxContainer/quality/af_value.text = str(int(pow(2.0, value)))
	ProjectSettings.set_setting(AF, pow(2.0, value))


func _on_shadows_toggled(button_pressed):
	emit_signal("shadows_toggled", button_pressed)


func log2(value: float) -> float:
	return log(value) / log(2.0)


func _on_back_pressed():
	emit_signal("quit")


func _input(event):
	if event.is_action_pressed("quit"):
		emit_signal("quit")
