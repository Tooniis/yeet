extends Control

export(float, 0.0, 60.0) var video_time

var loader: ResourceInteractiveLoader
var stage_count: float
var scene: Resource
onready var root = get_tree().get_root()


func _ready():
	loader = ResourceLoader.load_interactive("res://island/island.tscn")
	stage_count = loader.get_stage_count()


func _process(_delta):
	scene = loader.get_resource()
	if scene == null:
		poll()
	else:
		start()


func poll():
	loader.poll()
	$bar.value = (loader.get_stage()+1) / stage_count
	if $bar.value == 1.0:
		$picardia.visible = true


func start():
	root.remove_child(self)
	root.add_child(scene.instance())
	queue_free()
	Main.loaded()
