extends LineEdit

signal ip_obtained


func _on_enter_pressed():
	validate_ip(text)


func _on_ip_box_text_entered(text):
	validate_ip(text)


func validate_ip(text):
	print(text)
	var text_splitted = text.split(":")
	print(text_splitted)

	# Verify IP address and port
	var invalid_message = ""
	if text_splitted.size() != 2:
		if text_splitted.size() == 1:
			invalid_message = "Port missing"
		else:
			invalid_message = "Invalid input"
	else:
		# IP Address
		var ip_splitted = text_splitted[0].split(".")
		if ip_splitted.size() != 4:
			invalid_message = "Invalid IP address"
		else:
			for section in ip_splitted:
				if len(section) > 3:
					invalid_message = "Invalid IP address"
					break
				else:
					for c in section:
						if not c in "0123456789.":
							invalid_message = "Invalid IP address"
		# Port
		if invalid_message == "":
			for c in text_splitted[1]:
				if not c in "0123456789":
					invalid_message = "Invalid port"
				else:
					if int(text_splitted[1]) > 65535:
						invalid_message = "Invalid port"

	if invalid_message != "":
		print(invalid_message)
		$"../ip_invalid_message".text = invalid_message
	else:
		emit_signal("ip_obtained", text_splitted[0], int(text_splitted[1]))
