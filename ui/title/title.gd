extends Control

signal settings_requested


func _ready():
	pass


func _on_online_pressed():
	$buttons.visible = false
	$client.visible = true


func _on_back_pressed():
	$client.visible = false
	$buttons.visible = true


func _on_offline_button_pressed():
	pass # Replace with function body.


func _on_ip_box_ip_obtained(_ip, _port):
	$client.visible = false
	$name.visible = true


func _on_name_box_text_entered(_new_text):
	$name.visible = false


func _on_settings_pressed():
	emit_signal("settings_requested")
