extends InteractableItem

export(float) var switch_rotation
export(float, 0.0, 1.0) var rotate_duration

onready var switch = $"../switch"
onready var switch_origin = switch.rotation.x
var power := false setget set_power


func _input(event):
	if $HUD.visible: # if HUD is visible then player is in control
		if event.is_action_pressed("action_1"):
			request_power(not power)


func request_power(on: bool):
	rpc_id(1, "update_power", on)


remote func set_power(on: bool):
	power = on
	if on:
		$Tween.interpolate_property(
			switch, "rotation_degrees:x",
			null,
			switch_rotation,
			rotate_duration
		)
		$Tween.start()
		$audio.play()
	else:
		$Tween.interpolate_property(
			switch, "rotation:x",
			null,
			switch_origin,
			rotate_duration
		)
		$Tween.start()
		$audio.stop()


remote func send_position():
	rpc_unreliable_id(1, "update_position", $audio.get_playback_position())


remote func set_position(position: float):
	$Tween.interpolate_callback($audio, 0.2, "seek", position)
	$Tween.start()
	#$audio.seek(position)
	#print($audio.playing, $audio.get_playback_position(), position)


# Singleplayer only
sync func update_power(on: bool):
	self.power = on
